<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use App\Http\Requests\PostStore;
use App\Http\Requests\PostUpdate;
use App\Http\Resources\Post as PostResource;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $post = Post::all(); 
        return response()->json([
            'status_code' => ' 200 ',
            'message' => 'Yêu cầu thành công',
            'information' => $post,
        ]);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStore $request)
    {   
        $data = Post::create($request->validated());
        return response()->json([
            'status_code' => ' 200 ',
            'message' => 'Yêu cầu thành công',
            'information' => $data,
        ]);
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        $infor = new PostResource($post);
        return response()->json([
            'status code' => ' 200 ',
            'message' => 'Yêu cầu thành công',
            'information' => $infor,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, Post $post)
    // {   
    //     $post->update($request->all());
    // }
    // public function update(PostUpdate $request, Post $post)
    // {   
    //     $data = $request->validated();
    //     $post->update($data);
    // }
    public function update(PostUpdate $request, $id)
    {   
        $post = Post::findOrFail($id);
        $post->update($request->validated());
        return response()->json([
            'status code' => ' 200 ',
            'message' => 'Yêu cầu thành công',
            'information' => $post,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        return response()->json([
            'status code' => ' 200 ',
            'message' => 'Xoá thành công',
        ]);
    }
}
